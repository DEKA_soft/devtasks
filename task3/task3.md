# Task 3

In file ApplicationForm.aspx

- CSS styles in separate file
- JavaScript function in separate file

In file ApplicationForm.aspx.cs

- Don’t hardcoded contact information in the code, because this data can be changed. read contact list from a database or other sources in which it is possible to make a change of data without having to rebuild the code.
