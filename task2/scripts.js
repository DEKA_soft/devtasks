const app = document.getElementById("root");

const logo = document.createElement("img");
logo.src = "logo.png";

const container = document.createElement("div");
container.setAttribute("class", "container");

app.appendChild(logo);
app.appendChild(container);

var request = new XMLHttpRequest();
request.open("GET", "https://swapi.co/api/films", true);
request.onload = function() {
  // accessing JSON data here
  var data = JSON.parse(this.response);

  if (request.status >= 200 && request.status < 400) {
    // sort movies by id
    let movies = data.results.sort(function(a, b) {
      return parseInt(a.episode_id) - parseInt(b.episode_id);
    });
    // go through hole json array and show data in html
    movies.forEach(movie => {
      let card = document.createElement("div");
      card.setAttribute("class", "card");
      // movie title
      let h1 = document.createElement("h1");
      h1.textContent = movie.title;
      // episode_id
      let peid = document.createElement("p");
      peid.textContent = "Episode ID: " + movie.episode_id;
      // opening_crawl
      let p = document.createElement("p");
      p.textContent = movie.opening_crawl;
      // director
      let pd = document.createElement("p");
      pd.setAttribute("class", "syn");
      pd.textContent = "Director: " + movie.director;
      p.textContent = movie.opening_crawl;
      // producer
      let pro = document.createElement("p");
      pro.setAttribute("class", "syn");
      pro.textContent = "Producer: " + movie.producer;
      // release_date
      let pr = document.createElement("p");
      let releaseDate = new Date(movie.release_date);
      pr.textContent = "Release date: " + releaseDate.toLocaleDateString();
      // add elements to dom
      container.appendChild(card);
      card.appendChild(h1);
      card.appendChild(peid);
      card.appendChild(p);
      card.appendChild(pd);
      card.appendChild(pro);
      card.appendChild(pr);
    });
  } else {
    const errorMessage = document.createElement("marquee");
    errorMessage.textContent = "Not working!";
    app.appendChild(errorMessage);
  }
};

request.send();
