# Task 1

In the shown UI (Figure 1), we can see several elements: breadcrumbs, search, logo, main navigation, content buttons (options) and content of the page.

![Foo](figure1.png)

Figure 1 Original layout

The design for mobile devices could look like Figure 2. Where the elements are arranged vertically on the screen. The order of the elements from the top of the screen is as follows: the logo with the button to open the main menu, search options, breadcrumbs, content buttons, page content.

![Foo](figure2.png)

Figure 2 Suggested layout for mobile devices
